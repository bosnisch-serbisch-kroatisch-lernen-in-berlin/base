---
bg: 'FLAVOR-2.jpg'
layout: page
title: "Impressum"
crawlertitle: "Impressum"
permalink: /imprint/
---
#### Angaben gemäß §5 TMG

südost Europa Kultur e.V.  
Davorka Popadić-Schleicher  
Großbeerenstraße 88  
10963 Berlin–Kreuzberg

#### Vertreten durch die Verantwortlichen für den Inhalt nach § 55 Abs. 2 RStV:

Davorka Popadić-Schleicher  
E-Mail: [davorka_popadic-schleicher@web.de](mailto:davorka_popadic-schleicher@web.de)

#### Rechtliche Hinweise

Die Inhalte dieser Seite sind sorgfältig erstellt und werden regelmäßig aktualisiert. Es wird jedoch keine Gewähr für die Richtigkeit, Vollständigkeit, Aktualität und Verfügbarkeit der von uns bereitgestellten Informationen gegeben. Wir behalten uns vor die von uns bereitgestellten Inhalte jederzeit zu ändern.
Haftung für Links Dritter

#### Haftung für Links Dritter

Unser Internetauftritt enthält Verweise (Hyperlinks) zu externen Internetseiten Dritter. Auf deren Inhalte haben wir keinen Einfluss und übernehmen für diese keine Gewähr.
Urheberschutz

#### Urheberschutz

Die Texte und Werke dieses Internetauftrittes sind urheberrechtlich sowie durch andere gewerbliche Schutzrechte geschützt. Die Vervielfältigung und jede Art von Verwertung der Medieninhalte ist weder für eine kommerzielle Nutzung, noch für den Privatgebrauch oder eine andere, nicht kommerzielle Sache gestattet.
