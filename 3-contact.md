---
bg: "subway.jpg"
layout: page
title: "Kontakt"
crawlertitle: "Kontakt"
permalink: /contact/
active: about
---

#### Kontakt

Name: Davorka Popadić-Schleicher  
Telefon: [+49 157 53731311](mobil:+4915753731311)  
E-Mail: [davorka_popadic-schleicher@web.de](mailto:davorka_popadic-schleicher@web.de)  

#### Adresse

südost Europa Kultur e.V.  
Großbeerenstraße 88  
10963 Berlin–Kreuzberg

#### Anfahrt

<span class="lines">
  <span class="U1">U1</span> <span class="U3">U3</span> <span class="U7">U7</span>
</span>
<span class="station">Möckernbrücke</span>
<br />
<span class="lines">
  <span class="U1">U1</span> <span class="U3">U3</span> <span class="U6">U6</span>
</span>
<span class="station">Hallesches Tor</span>
<br />
<span class="lines">
  <span class="S1">S1</span> <span class="S2">S2</span> <span class="S25">S25</span> <span class="S26">S26</span>
</span>
<span class="station">Anhalter Bahnhof</span>

#### Karte

<iframe width="500" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=13.383973538875582%2C52.49868729293498%2C13.38750332593918%2C52.50019440993978&amp;layer=mapnik&amp;marker=52.499440041472184%2C13.38573843240738" style="width: 100%; height: 400px; margin: 2rem 0 0;"></iframe>
