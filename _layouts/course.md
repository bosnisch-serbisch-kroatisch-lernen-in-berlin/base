---
layout: page
---
<h2>{{ page.name }}</h2>

<strong>{{ page.schedule }}</strong>

<p>{{ page.description }}</p>

<h3>Kursdauer</h3>

<p>{{ page.duration }}</p>

<h3>Teilnehmende</h3>

<p>{{ page.participants }}</p>

<h3>Preis</h3>

<p>{{ page.price }}</p>

<h3><a href="/contact">Ort</a></h3>

<p>
{% for location in page.location %}
{{ location }}<br />
{% endfor %}
</p>

<h3>Lernmaterialien</h3>

<ul>
{% for material in page.materials %}
<li>{{ material }}</li>
{% endfor %}
</ul>
