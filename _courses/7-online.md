---
bg: "courses.jpg"
layout: course
title: "Kurse"
crawlertitle: "Kurse"
permalink: /courses/online/
active: courses
name: Onlinekurs
schedule: auf Anfrage
description: >-
  Je nach Zeitkapazitäten können unterschiedliche Onlinekurse mit individuellen Lerninhalten angeboten werden.
  Bitte <a href="/contact/">kontaktieren Sie mich</a> zur Vereinbarung eines Termins.
materials:
  - individuelle Materialien
duration: individuelle Kursdauer
location:
  - Der Kurs wird online abgehalten.
participants: 4 – 6 Teilnehmende
price: 200 €
---
