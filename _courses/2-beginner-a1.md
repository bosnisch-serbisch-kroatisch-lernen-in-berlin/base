---
bg: "courses.jpg"
layout: course
title: "Kurse"
crawlertitle: "Kurse"
permalink: /courses/beginner-a1/
active: courses
name: Einsteigerkurs (A1)
schedule: Dienstags, ab 27.02.2024 um 19:30 Uhr
description: >-
  Man lernt sich in einfachen Situationen zu verständigen und mit einfachen Mitteln die eigene Herkunft und Ausbildung, die direkte Umgebung und Dinge im Zusammenhang mit unmittelbaren Bedürfnissen zu beschreiben.
materials:
  - Dobro došli 1
duration: 15 Termine à 90 Minuten (30 Unterrichtsstunden)
location:
  - Der Kurs wird online abgehalten.
participants: 4 – 6 Teilnehmende
price: 200 €
---
