---
bg: "FLAVOR-1.jpg"
layout: page
summary: ""
---
{% if site.flavor == 'k' %}
## Möchten Sie Kroatisch in Berlin lernen?
{% else %}
## Möchten Sie Bosnisch, Serbisch oder Kroatisch in Berlin lernen?
{% endif %}

Das Südost-Zentrum in der Großbeerenstraße ist genau der richtige Ort dafür!

Lernen sie {% if site.flavor == 'k' %}Kroatisch{% else %}Bosnisch, Serbisch oder Kroatisch{% endif %} in angenehmer Atmosphäre.
Unsere Kleingruppen bestehen aus vier bis sieben Teilnehmern, von den Anfänger- bis zu den Konversationskursen.

Der Unterricht wird von Dozentin Davorka Popadić-Schleicher geleitet.
Die Muttersprachlerin ist eine erfahrene Sprachlehrerin, die ihr Diplom als Gymnasiallehrerin an der Universität Zagreb erworben hat.

{% if site.flavor == 'k' %}
Wussten Sie, dass Kroatien nur 4,5 Millionen Einwohner hat, aber über 1000 Inseln, 8 wunderschöne Nationalparks – und 3 Dialekte?

Nicht nur die kroatische Sprache, sondern auch die Kultur und die Geschichte des Landes lernen Sie in unseren Kursen kennen.
{% endif %}

Außer der Lehrbuchnutzung lesen wir in fortgeschrittenen Kursen Zeitungen und Bücher und schauen uns aktuelle Filme an.

**Kurse werden aufgrund der aktuellen Situation mit der Covid-19-Pandemie ausschließlich online abgehalten.**

{% for course in site.courses %}
#### [{{ course.name }}]({{ course.permalink | prepend: site.baseurl }})

{{ course.schedule }}
{% endfor %}

{% include mailchimp.html %}
