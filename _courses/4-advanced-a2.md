---
bg: "courses.jpg"
layout: course
title: "Kurse"
crawlertitle: "Kurse"
permalink: /courses/advanced-a2/
active: courses
name: Fortgeschrittenen Kurs (A2)
schedule: Montags, ab 26.02.2023 um 19:30 Uhr
description: >-
  Man kann sich einfach und zusammenhängend über vertraute Themen und persönliche Interessengebiete austauschen. Man lernt über Erfahrungen und Ereignisse zu berichten und die Hauptinhalte von  Zeitungsartikeln zu verstehen und zu diskutieren.
materials:
  - Dobro došli 2
duration: 15 Termine à 90 Minuten (30 Unterrichtsstunden)
location:
  - Der Kurs wird online abgehalten.
participants: 4 – 6 Teilnehmende
price: 200 €
---
