---
bg: "courses.jpg"
layout: page
title: "Kurse"
crawlertitle: "Kurse"
permalink: /courses/
active: courses
---
**Kurse werden aufgrund der aktuellen Situation mit der Covid-19-Pandemie ausschließlich online abgehalten.**

{% for course in site.courses %}
## [{{ course.name }}]({{ course.permalink | prepend: site.baseurl }})

**{{ course.schedule }}**

{{ course.description }}
{% endfor %}

{% include mailchimp.html %}
