---
published: false
bg: "courses.jpg"
layout: course
title: "Kurse"
crawlertitle: "Kurse"
permalink: /courses/conversation/
active: courses
name: Konversationskurs
schedule: Montags, ab 27.02.2023 um 19:00 Uhr
description: >-
  Man kann sich spontan und fließend verständigen, so dass eine Konversation mit Muttersprachlern ohne Anstrengung auf beiden Seiten gut möglich ist. Man lernt sich zu einem breiten Themenspektrum klar und detailliert zu äußern, einen Standpunkt zu einer aktuellen Frage zu erläutern und Vor-und Nachteile verschiedener Möglichkeiten anzugeben.
materials:
  - Tageszeitungen Jutarnji list, Večernji list, Vjesnik, Novi list, Slobodna Dalmacija
  - Kroatische Filme
  - Kurzgeschichten von kroatischen Autoren wie Ante Tomić, Miljenko Jergović, Edo Popović, Slavenka Drakulić und Dubravka Ugreši
duration: 15 Termine à 90 Minuten (30 Unterrichtsstunden)
location:
  - Der Kurs wird online abgehalten.
participants: 4 – 6 Teilnehmende
price: 200 €
---
