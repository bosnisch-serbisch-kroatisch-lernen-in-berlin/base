---
bg: "books.jpg"
layout: page
title: "Über mich"
crawlertitle: "Über mich"
permalink: /about/
active: about
---
## Zur Person

Davorka Popadić-Schleicher ist eine Dozentin im Südost Zentrum.
Als Muttersprachlerin leitet sie dort Kurse für die Kroatische Sprache.
Ihren Abschluss in Linguistik für die kroatische und serbische Sprache hat sie an der Universität Zagreb erlangt und verfügt über eine langjährige berufliche Erfahrung in diesem Feld.

Während ihrer beruflichen Praxis hat sie unter anderem Angehörige der Bundesbehörden, wie zum Beispiel der Polizei und dem Außenministerium, Doktoranden sowie Studenten unterrichtet.

Sie schrieb für den kroatischen „Vjesnik“ und schrieb und übersetzte für das monatliche aufgelegte Deutsch-Kroatische Kulturmagazin „Ritam“.

Zur Zeit ist sie als Beraterin für verschiedene Produktionen des deutschen Fernsehens (z.B. für die Redaktion des „Tatort“) tätig.

## Werdegang

|----------------|--------------------------------------------------------------------------------------------------------------------------------------------------|
| 2000/05        | Ministerium für Bildung und Sport der Republik Kroatien: Schulunterricht der kroatischen Sprache und Geschichte in der Kroatischen Schule Berlin |
| 2003/04        | Botschaft der Republik Kroatien in Berlin: Kursleitung „Kroatisch als Fremdsprache“                                                              |
| seit&nbsp;2005 | Südost Zentrum e.V. Berlin: Leitung des Kurs „Kroatisch oder Serbisch als Fremdsprache“ intensive Zusammenarbeit mit „Vjesnik“, Zagreb           |
| 2005/07        | Redaktionelle Mitarbeit beim deutsch-kroatischen Kulturmagazin für Berlin „Ritam“                                                                |
| seit&nbsp;2007 | Lernbrücke e.V. Berlin: Leitung des Kurs „Kroatisch oder Serbisch als Fremdsprache“                                                              |
| 2010/12        | Sprachstudio Babelsberg: Leitung des Kurses „Kroatisch als Fremdsprache“                                                                         |
| seit&nbsp;2013 | SLZ - Sprachlernzentrum im Auswärtigen Amt: Kursleitung und Einzelunterricht für Bosnisch, Kroatisch und Serbisch                                |
| 2014/2018      | Friedrich-Ebert-Stiftung: Einzelunterricht Kroatisch und Bosnisch als Fermdsprache                                                               |
| 2016           | Bundespolizeiamt: Einzelunterricht Serbisch als Fremdsprache                                                                                     |
| 2018           | Konrad-Adenauer- Stiftung: Einzelunterrischt Kroatisch als Fremdsprache                                                                          |
| 2020/2021      | Deutscher Bundestag: Einzelunterricht Serbisch als Fremdsprache                                                                                  |
| 2021           | Friedrich-Ebert-Stiftung: Einzelunterricht Kroatisch als Fremdsprache                                                                            |
| 2022           | Friedrich-Naumann-Stiftung für Freiheit: Einzelunterricht Serbisch als Fremdsprache                                                              |
| 2022           | VERSschmuggel: Ein Projekt für das Haus für Poesie Berlin                                                                                        |
| 2022/2023      | Bundespolizeiamt: Einzelunterricht Kroatisch als Fremdsprache                                                                                    |
