---
published: false
bg: "courses.jpg"
layout: course
title: "Kurse"
crawlertitle: "Kurse"
permalink: /courses/beginner-a1-a2/
active: courses
name: Einsteigerkurs (A1 – A2)
schedule: Dienstags, ab 28.02.2023 um 19:30 Uhr
description: >-
  Man lernt sich in einfachen Situationen zu verständigen und mit einfachen Mitteln die eigene Herkunft und Ausbildung, die direkte Umgebung und Dinge im Zusammenhang mit unmittelbaren Bedürfnissen zu beschreiben.
  Man kann sich einfach und zusammenhängend über vertraute Themen und persönliche Interessengebiete austauschen. Man lernt über Erfahrungen und Ereignisse zu berichten und die Hauptinhalte von  Zeitungsartikeln zu verstehen und zu diskutieren.
materials:
  - Dobro došli 1
  - Dobro došli 2
duration: 15 Termine à 90 Minuten (30 Unterrichtsstunden)
location:
  - Der Kurs wird online abgehalten.
participants: 4 – 6 Teilnehmende
price: 200 €
---
